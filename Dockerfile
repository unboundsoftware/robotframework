FROM python:3.13.2-slim@sha256:f3614d98f38b0525d670f287b0474385952e28eb43016655dd003d0e28cf8652

ARG TARGETOS
ARG TARGETARCH
ARG GECKODRIVER_VERSION=0.32.0

RUN install -d -m 0755 /etc/apt/keyrings && \
    apt update -qq && \
    apt -y -qq install wget gnupg2 jq curl git fonts-liberation libdrm2 libgbm1 libnspr4 libnss3 libu2f-udev libvulkan1 nodejs npm xdg-utils && \
    wget -q https://packages.mozilla.org/apt/repo-signing-key.gpg -O- | tee /etc/apt/keyrings/packages.mozilla.org.asc > /dev/null && \
    gpg -n -q --import --import-options import-show /etc/apt/keyrings/packages.mozilla.org.asc | awk '/pub/{getline; gsub(/^ +| +$/,""); print "\n"$0"\n"}' && \
    echo "deb [signed-by=/etc/apt/keyrings/packages.mozilla.org.asc] https://packages.mozilla.org/apt mozilla main" | tee -a /etc/apt/sources.list.d/mozilla.list > /dev/null && \
    printf 'Package: *\nPin: origin packages.mozilla.org\nPin-Priority: 1000' >/etc/apt/preferences.d/mozilla && \
    apt update -qq && \
    apt -y -qq install firefox && \
    wget -q https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb && \
    dpkg -i google-chrome-stable_current_amd64.deb && \
    rm -r /var/lib/apt/lists/* google-chrome-stable_current_amd64.deb

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt && \
    rm requirements.txt && \
    rfbrowser init
